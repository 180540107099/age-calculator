package com.aswdc.agecalculator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.agecalculator.R;
import com.aswdc.agecalculator.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;
    OnViewClickListener onViewClickListener;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;
    }

    public interface OnViewClickListener {
        void OnDeleteClick(int position);

        void OnItemClick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }


    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.tvName.setText(userList.get(position).getName());
        holder.tvRelation.setText(userList.get(position).getRelation());
        holder.tvAgeInNumber.setText(userList.get(position).getDate());

        holder.tvAgeInNumber.setText(String.valueOf(userList.get(position).getAge()));

        holder.itemView.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvRelation)
        TextView tvRelation;
        @BindView(R.id.tvAgeInNumber)
        TextView tvAgeInNumber;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
