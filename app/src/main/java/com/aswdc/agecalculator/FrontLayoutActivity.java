package com.aswdc.agecalculator;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.aswdc.agecalculator.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FrontLayoutActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.CvActCalculateAge)
    CardView CvActCalculateAge;
    @BindView(R.id.CvActDateDifference)
    CardView CvActDateDifference;
    @BindView(R.id.CvActFamilyProfile)
    CardView CvActFamilyProfile;
    @BindView(R.id.CvActCompareAge)
    CardView CvActCompareAge;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frontlayout);
        setUpActionBar("Family Age Calculator", true);
        ButterKnife.bind(this);
        new MyDatabase(this).getWritableDatabase();
    }


    @OnClick(R.id.CvActCalculateAge)
    public void onCvActCalculateAgeClicked() {
        Intent intent = new Intent(this, CalculateAgeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.CvActDateDifference)
    public void onCvActDateDifferenceClicked() {
        Intent intent = new Intent(this, DateDifferenceActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.CvActFamilyProfile)
    public void onCvActFamilyProfileClicked() {
        Intent intent = new Intent(this, FamilyProfileActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.CvActCompareAge)
    public void onCvActCompareAgeClicked() {
        Intent intent = new Intent(this, CompareAgeActivity.class);
        startActivity(intent);
    }
}
