package com.aswdc.agecalculator.model;

import java.io.Serializable;

public class UserModel implements Serializable {

    int UserID;
    String Name;
    String Relation;
    String Date;
    int Age;

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserID=" + UserID +
                ", Name='" + Name + '\'' +
                ", Relation='" + Relation + '\'' +
                ", Date='" + Date + '\'' +
                ", Age=" + Age +
                '}';
    }
}
