package com.aswdc.agecalculator;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.joda.time.PeriodType;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateDifferenceActivity extends BaseActivity {

    DatePickerDialog.OnDateSetListener dateSetListener1,dateSetListener2;
    EditText eText1,eText2;
    Button btnGet;
    TextView tvYear, tvMonths, tvDays;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datedifference);
        setUpActionBar("Date Difference", true);

        tvYear = findViewById(R.id.tvResultYear);
        tvMonths = findViewById(R.id.tvResultMonths);
        tvDays = findViewById(R.id.tvResultDays);
        eText1 = findViewById(R.id.etStart);
        eText2 = findViewById(R.id.etEnd);

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date = simpleDateFormat.format(Calendar.getInstance().getTime());

        eText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(DateDifferenceActivity.this, dateSetListener1, year, month, day);
                datePickerDialog.show();
            }
        });

        dateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                eText1.setText(String.format("%02d",day) + "-" + String.format("%02d",(month + 1)) + "-" + year);
            }
        };

        eText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(DateDifferenceActivity.this, dateSetListener2, year, month, day);
                datePickerDialog.show();
            }
        });

        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                eText2.setText(String.format("%02d",day) + "-" + String.format("%02d",(month + 1)) + "-" + year);
            }
        };

        btnGet = findViewById(R.id.btnCalculate);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sDate = eText1.getText().toString();
                String eDate = eText2.getText().toString();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                if (eText1.getText().toString().equals("") && eText2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Select Date",Toast.LENGTH_SHORT).show();
                }
                else if (eText1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Select Date",Toast.LENGTH_SHORT).show();
                }
                else if (eText2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),"Select Date",Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        Date date1 = simpleDateFormat.parse(sDate);
                        Date date2 = simpleDateFormat.parse(eDate);

                        long startDate = date1.getTime();
                        long endDate = date2.getTime();

                        if (startDate <= endDate)
                        {
                            org.joda.time.Period period = new org.joda.time.Period(startDate,endDate, PeriodType.yearMonthDay());
                            int years = period.getYears();
                            int months = period.getMonths();
                            int days = period.getDays();

                            tvYear.setText(" " +years);
                            tvMonths.setText(" " +months);
                            tvDays.setText(" " +days);
                        }

                        else
                        {
                            Toast.makeText(getApplicationContext(),"From Date should not be greater than To Date",Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Button btn = findViewById(R.id.btnClear);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText eText1 = findViewById(R.id.etStart);
                EditText eText2 = findViewById(R.id.etEnd);
                TextView tvYear = findViewById(R.id.tvResultYear);
                TextView tvMonths = findViewById(R.id.tvResultMonths);
                TextView tvDays = findViewById(R.id.tvResultDays);
                eText1.setText("");
                eText2.setText("");
                tvYear.setText("");
                tvMonths.setText("");
                tvDays.setText("");
            }
        });
    }
}
