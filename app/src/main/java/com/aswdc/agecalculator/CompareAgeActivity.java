package com.aswdc.agecalculator;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.joda.time.PeriodType;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CompareAgeActivity extends BaseActivity {

    DatePickerDialog picker;
    EditText eText1,eText2,eText3,eText4;
    Button btnGet;
    TextView tvResult, tvFirstPerson, tvSecondPerson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compareage);
        setUpActionBar("Compare Age", true);

        tvResult = (TextView) findViewById(R.id.tvDisplayResult);
        tvFirstPerson = (TextView) findViewById(R.id.tvDisplayFirstPerson);
        tvSecondPerson = (TextView) findViewById(R.id.tvDisplaySecondPerson);

        //eText3.setInputType(InputType.TYPE_NULL);
        //eText4.setInputType(InputType.TYPE_NULL);

        eText1 = (EditText) findViewById(R.id.etDate1);
        eText1.setInputType(InputType.TYPE_NULL);
        eText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(CompareAgeActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText1.setText(String.format("%02d",dayOfMonth) + "-" + String.format("%02d",(monthOfYear + 1)) + "-" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        eText2 = (EditText) findViewById(R.id.etDate2);
        eText2.setInputType(InputType.TYPE_NULL);
        eText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(CompareAgeActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText2.setText(String.format("%02d",dayOfMonth) + "-" + String.format("%02d",(monthOfYear + 1)) + "-" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        btnGet = (Button) findViewById(R.id.btnCalculate);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sDate = eText1.getText().toString();
                String eDate = eText2.getText().toString();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                if (eText1.getText().toString().equals("") && eText2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Date", Toast.LENGTH_SHORT).show();
                } else if (eText1.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Date", Toast.LENGTH_SHORT).show();
                } else if (eText2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Date", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Date date1 = simpleDateFormat.parse(sDate);
                        Date date2 = simpleDateFormat.parse(eDate);

                        long startDate = date1.getTime();
                        long endDate = date2.getTime();

                        if (startDate <= endDate) {
                            org.joda.time.Period period = new org.joda.time.Period(startDate, endDate, PeriodType.yearMonthDay());
                            int years = period.getYears();
                            int months = period.getMonths();
                            int days = period.getDays();

                            tvFirstPerson.setText("First Person is ");
                            tvResult.setText(String.format("%02d",years) + " Year " +String.format("%02d",months) + " Months " +String.format("%02d",days) + " Days ");
                            tvSecondPerson.setText("Older than Second Person");
                        }

                        if (startDate >= endDate){
                            org.joda.time.Period period = new org.joda.time.Period(endDate, startDate, PeriodType.yearMonthDay());
                            int years = period.getYears();
                            int months = period.getMonths();
                            int days = period.getDays();

                            tvFirstPerson.setText("Second Person is ");
                            tvResult.setText(String.format("%02d",years) + " Year " +String.format("%02d",months) + " Months " +String.format("%02d",days) + " Days ");
                            tvSecondPerson.setText("Older than First Person");
                        }

                        if (startDate == endDate){
                            tvFirstPerson.setText(null); tvSecondPerson.setText(null);
                            tvResult.setText("Age of First Person is similar to that of Second Person");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Button btn=(Button) findViewById(R.id.btnClear);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText eText1=(EditText) findViewById(R.id.etDate1);
                EditText eText2=(EditText) findViewById(R.id.etDate2);
                EditText eText3=(EditText) findViewById(R.id.etName1);
                EditText eText4=(EditText) findViewById(R.id.etName2);
                TextView tvResult=(TextView) findViewById(R.id.tvDisplayResult);
                eText1.setText("");
                eText2.setText("");
                eText3.setText("");
                eText4.setText("");
                tvFirstPerson.setText("");
                tvSecondPerson.setText("");
                tvResult.setText("");
            }
        });
    }
}
