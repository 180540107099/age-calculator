package com.aswdc.agecalculator.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.aswdc.agecalculator.model.UserModel;

import java.util.ArrayList;
import java.util.Calendar;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserID";
    public static final String NAME = "Name";
    public static final String RELATION = "Relation";
    public static final String DATE = "Date";
    public static final String AGE = "Age";

    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        Log.d("userList::1::",""+ cursor.getCount());
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    private UserModel getCreatedModelUsingCursor(Cursor cursor) {
        UserModel model = new UserModel();
        model.setUserID(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setRelation(cursor.getString(cursor.getColumnIndex(RELATION)));
        model.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
        model.setAge(cursor.getInt(cursor.getColumnIndex(AGE)));
        return model;
    }

    public UserModel getUserByID(int ID) {
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(ID)});
        cursor.moveToFirst();
        model = getCreatedModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return model;
    }

    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();
        return ageS;
    }

    public long insertUserByID(String name, String relation, String date, int tempDay, int tempMonth, int tempYear) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(RELATION, relation);
        cv.put(DATE, date);
        cv.put(AGE,Integer.valueOf(getAge(tempYear,tempMonth,tempDay)));

        long lastInsertedID = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedID;
    }

    public int updateUserByID(String name, String relation, String date, int tempDay, int tempMonth, int tempYear, int userID) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(RELATION, relation);
        cv.put(DATE, date);
        cv.put(AGE,Integer.valueOf(getAge(tempYear,tempMonth,tempDay)));
        int lastUpdatedID = db.update(TABLE_NAME, cv, USER_ID + " = ?", new String[]{String.valueOf(userID)});
        db.close();
        return lastUpdatedID;
    }

    public int deleteUserByID(int userID) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserID = db.delete(TABLE_NAME, USER_ID + " = ?", new String[]{String.valueOf(userID)});
        db.close();
        return deletedUserID;
    }
}
